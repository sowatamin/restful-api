<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        Permission::create(['name' => 'store book']);
        Permission::create(['name' => 'show book']);
        Permission::create(['name' => 'update book']);
        Permission::create(['name' => 'delete book']);
        Permission::create(['name' => 'admin settings']);

        $role = Role::create(['name' => 'user'])
            ->givePermissionTo([
                'store book',
                'show book',
                'update book',
                'delete book',
            ]);

        $role = Role::create(['name' => 'admin']);
        $role->givePermissionTo(Permission::all());
    }
}
