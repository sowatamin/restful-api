<?php

namespace App\Http\Controllers;

use App\Book;
use App\Http\Resources\BookResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index()
    {
        $user = Auth::user();
        $user_role = Role::find($user->role_id);
         if($user_role->hasPermissionTo('show book')){
             return BookResource::collection(Book::with('ratings')->paginate(25));
         } else{
             $message = "User dont have the permission to view the data";
             $status = 403;
             return response()->json(["message"=>$message,"status"=>$status],$status);
         }

    }

    public function store(Request $request)
    {
        $user = Auth::user();
        $user_role = Role::find($user->role_id);
        if($user_role->hasPermissionTo('store book')) {
            $this->validate($request, [
                'title' => 'required',
                'description' => 'required',
                'author' => 'required',
            ]);
            $book = new Book;
            $book->user_id = $user->id;
            $book->title = $request->title;
            $book->description = $request->description;
            $book->author = $request->author;
            $book->save();

            return new BookResource($book);
        } else{
            $message = "User dont have the permission to store the data";
            $status = 403;
            return response()->json(["message"=>$message,"status"=>$status],$status);
        }

    }

    public function show(Book $book)
    {
        $user = Auth::user();
        $user_role = Role::find($user->role_id);
        if($user_role->hasPermissionTo('show book')){
            return new BookResource($book);
        } else{
            $message = "User dont have the permission to view the data";
            $status = 403;
            return response()->json(["message"=>$message,"status"=>$status],$status);
        }

    }

    public function update(Request $request,Book $book )
    {
        //return response()->json($request);
        $user = Auth::user();
        $user_role = Role::find($user->role_id);
        if($user_role->hasPermissionTo('update book')){
            if ($user->id !== $book->user_id) {

                return response()->json(['error' => 'You can only edit your own books.'], 403);
            }
            $book->update($request->only(['title','author', 'description']));
            return new BookResource($book);
        } else{
            $message = "User dont have the permission to update the data";
            $status = 403;

            return response()->json(["message"=>$message,"status"=>$status],$status);

        }

    }

    public function destroy(Request $request,Book $book)
    {
        $user = Auth::user();
        $user_role = Role::find($user->role_id);
        if($user_role->hasPermissionTo('delete book')){
            if(is_null($book)){
                return response()->json(['error' => 'No records found.'], 404);

            } elseif($request->user()->id != $book->user_id){

                return response()->json(['error' => 'You can only delete your own books.'], 403);

            }
            $book->delete();

            return response()->json([
                "message"=>'Item removed successfully',
                "status"=>'204'
            ],204);

        } else{
            $message = "User dont have the permission to delete the data";
            $status = 403;

            return response()->json(["message"=>$message,"status"=>$status],$status);

        }

    }

    public function showTrash(Request $request)
    {
        $user = Auth::user();
        $user_role = Role::find($user->role_id);
        if($user_role->hasPermissionTo('delete book')){
            $trashed_book = Book::onlyTrashed()
                ->where('user_id',$user->id)
                ->get();
            if(is_null($trashed_book)){
                return response()->json([
                    "message"=>'No items in the trash',
                    "status"=>'204'
                ],204);
            }

            return response()->json([
                "message"=>'Trashed Data',
                "data"=>$trashed_book,
                "status"=>200],200);

        } else{
            $message = "User dont have the permission to view the data";
            $status = 403;

            return response()->json(["message"=>$message,"status"=>$status],$status);

        }


    }

    public function restoreBook(Request $request)
    {
        $user = Auth::user();
        $user_role = Role::find($user->role_id);
        if($user_role->hasPermissionTo('delete book')){
            $restore_book = Book::onlyTrashed()
                ->where('id',$request->book_id)
                ->where('user_id',$user->id)
                ->first();
            if(is_null($restore_book)){

                return response()->json(["message"=>'Record not found',"status"=>404],404);

            }
            $restore_book->restore();

            return response()->json([
                "message"=>'Record restored successfully',
                "data"=>$restore_book,
                "status"=>200],200);

        } else{
            $message = "User dont have the permission to restore the data";
            $status = 403;

            return response()->json(["message"=>$message,"status"=>$status],$status);
        }

    }

}
