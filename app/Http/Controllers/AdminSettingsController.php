<?php

namespace App\Http\Controllers;

use App\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class AdminSettingsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function add_user(Request $request)
    {
        $user = Auth::user();
        $role = Role::find($user->role_id);
        if($role->name == 'admin')
        {
            $this->validate($request, [
                'name' => 'required',
                'email' => 'required|unique:users',
                'password' => 'required',
            ]);

            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => bcrypt($request->password),
            ]);

            return response()->json([
                "messagee"=>'User added successfully',
                "data"=>$user,
                "status"=>200],200);

        }
        $message = "Only Admins are allowed to add user";
        $status = 403;
        return response()->json(["message"=>$message,"status"=>$status],$status);
    }

    public function removeUser(Request $request)
    {
        //return response()->json($request);

        $user = Auth::user();
        $role = Role::find($user->role_id);
        if($role->name == 'admin'){
            $data = User::find($request->user_id);
            if(is_null($data))
            {
                $message = "Record not found";
                $status = 404;
                return response()->json(["message"=>$message,"status"=>$status],$status);
            }
            $data->delete();

            return response()->json([
                "messagee"=>'User removed successfully',
                "data"=>$data,
                "status"=>200],200);
        }

        $message = "Only Admins are allowed to remove user";
        $status = 403;
        return response()->json(["message"=>$message,"status"=>$status],$status);

    }

    public function changePermission(Request $request)
    {
        $admin = Auth::user();
        //return response()->json($request);
        $admin_role = Role::find($admin->role_id);
        if($admin_role->name == 'admin'){
            $user = User::find($request->user_id);
            $user_role = Role::find($user->role_id);
            if(is_null($user_role)){
                $message = "Record not found";
                $status = 404;
                return response()->json(["message"=>$message,"status"=>$status],$status);
            } else{
                $permissions = Permission::all();
                $user_role->revokePermissionTo($permissions);
                $user_permission = Permission::whereIn('id',$request->permissions)->get();
                $user_role->givePermissionTo($user_permission);

                $message = "Settings were saved succesfully";
                $status = "200";
                return response()->json(["message"=>$message,"status"=>$status],$status);
            }
        } else{
            $message = "Only Admins are allowed to remove user";
            $status = 403;
            return response()->json(["message"=>$message,"status"=>$status],$status);
        }

    }


}
