<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('register', 'AuthController@register');
Route::post('login', 'AuthController@login');
Route::get('logout', 'AuthController@logout');
Route::get('user', 'AuthController@getAuthUser');
Route::apiResource('books', 'BookController');
Route::post('books/trash','BookController@showTrash');
Route::post('books/restore','BookController@restoreBook');
Route::post('books/{book}/ratings', 'RatingController@store')->middleware('auth:api');
//Admin
Route::post('admin/settings/add_user', 'AdminSettingsController@add_user');
Route::post('admin/settings/user/permissions', 'AdminSettingsController@changePermission');
Route::delete('admin/settings/remove_user', 'AdminSettingsController@removeUser');

